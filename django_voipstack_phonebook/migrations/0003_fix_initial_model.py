# Generated by Django 3.0.2 on 2020-03-04 13:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_voipstack_phonebook", "0002_fix_initial_model"),
    ]

    operations = [
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="number_type",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoiceNumberType",
            ),
        ),
    ]
