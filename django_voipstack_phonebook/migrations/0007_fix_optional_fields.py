# Generated by Django 3.0.2 on 2020-03-05 15:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_voipstack_phonebook", "0006_add_name_additions"),
    ]

    operations = [
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="company_description",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoiceCompDescr",
            ),
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="hist_name",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoiceHistName",
            ),
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="name_title",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoiceTitle",
            ),
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="prefix_lastname",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoicePfxLn",
            ),
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="usage",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoiceUsage",
            ),
        ),
    ]
