from django.db import models

from django_voipstack_mdat.django_voipstack_mdat.models import VoipMdatNumbers


class VoipPhonebookData(models.Model):
    id = models.BigAutoField(primary_key=True)
    number = models.ForeignKey(VoipMdatNumbers, models.CASCADE, blank=False, null=False)
    extension = models.IntegerField(blank=True, null=True)
    company_description = models.ForeignKey(
        "VoipPhonebookChoiceCompDescr", models.CASCADE, blank=True, null=True
    )
    usage = models.ForeignKey(
        "VoipPhonebookChoiceUsage", models.CASCADE, blank=False, null=False, default=1
    )
    print_allowed = models.ForeignKey(
        "VoipPhonebookChoicePrint", models.CASCADE, blank=False, null=False, default=2
    )
    digital_allowed = models.ForeignKey(
        "VoipPhonebookChoiceDigital", models.CASCADE, blank=False, null=False, default=2
    )
    hotline_allowed = models.ForeignKey(
        "VoipPhonebookChoiceHotline", models.CASCADE, blank=False, null=False, default=2
    )
    name = models.CharField(max_length=120)
    firstname = models.CharField(max_length=50, blank=True, null=True)
    hist_name = models.ForeignKey(
        "VoipPhonebookChoiceHistName", models.CASCADE, blank=True, null=True
    )
    prefix_lastname = models.ForeignKey(
        "VoipPhonebookChoicePfxLn", models.CASCADE, blank=True, null=True
    )
    name_title = models.ForeignKey(
        "VoipPhonebookChoiceTitle", models.CASCADE, blank=True, null=True
    )
    street = models.CharField(max_length=40)
    house_nr = models.IntegerField()
    house_add = models.CharField(max_length=6, blank=True, null=True)
    zipcode = models.CharField(max_length=5)
    city = models.CharField(max_length=40)
    city_add = models.CharField(max_length=40, blank=True, null=True)
    number_type = models.ForeignKey(
        "VoipPhonebookChoiceNumberType", models.CASCADE, blank=False, null=False
    )
    inverse_search_allowed = models.BooleanField(default=True)
    tag = models.ForeignKey(
        "VoipPhonebookChoiceTag", models.CASCADE, blank=True, null=True
    )
    number_blocked = models.BooleanField(default=False)
    name_add = models.ForeignKey(
        "VoipPhonebookChoiceNameAdd", models.CASCADE, blank=True, null=True
    )

    def __str__(self):
        full_name = list()

        if self.name:
            full_name.append(self.name)

        if self.name_add:
            full_name.append(self.name_add.value)

        if self.firstname:
            full_name.append(self.firstname)

        if self.hist_name:
            full_name.append(self.hist_name.value)

        if self.prefix_lastname:
            full_name.append(self.prefix_lastname)

        if self.name_title:
            full_name.append(self.name_title)

        if self.company_description:
            full_name.append(self.company_description.value)

        if self.city:
            full_name.append(self.city)

        if self.street:
            full_name.append(self.street)

        if self.house_nr:
            full_name.append(str(self.house_nr))

        if self.house_add:
            full_name.append(self.house_add)

        return ", ".join(full_name)

    class Meta:
        db_table = "voip_phonebook_data"


class VoipPhonebookChoiceCompDescr(models.Model):
    value = models.CharField(max_length=80, unique=True, blank=False, null=False)

    def __str__(self):
        return self.value

    class Meta:
        db_table = "voip_phonebook_choice_comp_descr"


class VoipPhonebookChoiceUsage(models.Model):
    value = models.CharField(max_length=1, unique=True, blank=False, null=False)
    description = models.CharField(max_length=80, blank=False, null=False)

    def __str__(self):
        return self.value + " - " + self.description

    class Meta:
        db_table = "voip_phonebook_choice_usage"


class VoipPhonebookChoicePrint(models.Model):
    value = models.CharField(max_length=1, unique=True, blank=False, null=False)
    description = models.CharField(max_length=80, blank=False, null=False)

    def __str__(self):
        return self.value + " - " + self.description

    class Meta:
        db_table = "voip_phonebook_choice_print"


class VoipPhonebookChoiceDigital(models.Model):
    value = models.CharField(max_length=1, unique=True, blank=False, null=False)
    description = models.CharField(max_length=80, blank=False, null=False)

    def __str__(self):
        return self.value + " - " + self.description

    class Meta:
        db_table = "voip_phonebook_choice_digital"


class VoipPhonebookChoiceHotline(models.Model):
    value = models.CharField(max_length=1, unique=True, blank=False, null=False)
    description = models.CharField(max_length=80, blank=False, null=False)

    def __str__(self):
        return self.value + " - " + self.description

    class Meta:
        db_table = "voip_phonebook_choice_hotline"


class VoipPhonebookChoiceHistName(models.Model):
    value = models.CharField(max_length=30, unique=True, blank=False, null=False)

    def __str__(self):
        return self.value

    class Meta:
        db_table = "voip_phonebook_choice_hist_name"


class VoipPhonebookChoicePfxLn(models.Model):
    value = models.CharField(max_length=10, unique=True, blank=False, null=False)

    def __str__(self):
        return self.value

    class Meta:
        db_table = "voip_phonebook_choice_pfx_ln"


class VoipPhonebookChoiceTitle(models.Model):
    value = models.CharField(max_length=45, unique=True, blank=False, null=False)

    def __str__(self):
        return self.value

    class Meta:
        db_table = "voip_phonebook_choice_title"


class VoipPhonebookChoiceNumberType(models.Model):
    value = models.CharField(max_length=1, unique=True, blank=False, null=False)
    description = models.CharField(max_length=80, blank=False, null=False)

    def __str__(self):
        return self.value + " - " + self.description

    class Meta:
        db_table = "voip_phonebook_choice_number_type"


class VoipPhonebookChoiceTag(models.Model):
    value = models.CharField(max_length=50, unique=True, blank=False, null=False)

    def __str__(self):
        return self.value

    class Meta:
        db_table = "voip_phonebook_choice_tag"


class VoipPhonebookChoiceNameAdd(models.Model):
    value = models.CharField(max_length=5, unique=True, blank=False, null=False)

    def __str__(self):
        return self.value

    class Meta:
        db_table = "voip_phonebook_choice_name_add"


class VoipPhonebookPartnerDeteme(models.Model):
    entry = models.ForeignKey(
        "VoipPhonebookData", models.CASCADE, unique=True, blank=False, null=False
    )
    data_id = models.CharField(max_length=9, blank=False, null=False)

    def __str__(self):
        return "DTM Deutsche Tele Medien GmbH" + " - " + self.entry.__str__()

    class Meta:
        db_table = "voip_phonebook_partner_deteme"


class VoipPhonebookPartner(models.Model):
    name = models.CharField(max_length=100, unique=True, blank=False, null=False)
    sequence = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "voip_phonebook_partner"
