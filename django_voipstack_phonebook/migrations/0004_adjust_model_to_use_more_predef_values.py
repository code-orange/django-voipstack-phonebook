# Generated by Django 3.0.2 on 2020-03-05 15:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_voipstack_phonebook", "0003_fix_initial_model"),
    ]

    operations = [
        migrations.CreateModel(
            name="VoipPhonebookChoiceCompDescr",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("value", models.CharField(max_length=80)),
                ("description", models.CharField(max_length=80)),
            ],
            options={
                "db_table": "voip_phonebook_choice_comp_descr",
            },
        ),
        migrations.CreateModel(
            name="VoipPhonebookChoiceHistName",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("value", models.CharField(max_length=30)),
                ("description", models.CharField(max_length=80)),
            ],
            options={
                "db_table": "voip_phonebook_choice_hist_name",
            },
        ),
        migrations.CreateModel(
            name="VoipPhonebookChoicePfxLn",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("value", models.CharField(max_length=10)),
                ("description", models.CharField(max_length=80)),
            ],
            options={
                "db_table": "voip_phonebook_choice_pfx_ln",
            },
        ),
        migrations.CreateModel(
            name="VoipPhonebookChoiceTitle",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("value", models.CharField(max_length=45)),
                ("description", models.CharField(max_length=80)),
            ],
            options={
                "db_table": "voip_phonebook_choice_title",
            },
        ),
        migrations.CreateModel(
            name="VoipPhonebookPartnerDeteme",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("data_id", models.CharField(max_length=9)),
                (
                    "entry",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_voipstack_phonebook.VoipPhonebookData",
                    ),
                ),
            ],
            options={
                "db_table": "voip_phonebook_partner_deteme",
            },
        ),
        migrations.AddField(
            model_name="voipphonebookdata",
            name="prefix_lastname",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoicePfxLn",
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="company_description",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoiceCompDescr",
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="hist_name",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoiceHistName",
            ),
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="name_title",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoiceTitle",
            ),
        ),
    ]
