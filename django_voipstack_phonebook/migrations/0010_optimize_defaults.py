# Generated by Django 3.0.2 on 2020-03-05 23:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_voipstack_phonebook", "0009_add_partner_options"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="voipphonebookchoicecompdescr",
            name="description",
        ),
        migrations.RemoveField(
            model_name="voipphonebookchoicehistname",
            name="description",
        ),
        migrations.RemoveField(
            model_name="voipphonebookchoicenameadd",
            name="description",
        ),
        migrations.RemoveField(
            model_name="voipphonebookchoicepfxln",
            name="description",
        ),
        migrations.RemoveField(
            model_name="voipphonebookchoicetag",
            name="description",
        ),
        migrations.RemoveField(
            model_name="voipphonebookchoicetitle",
            name="description",
        ),
        migrations.AlterField(
            model_name="voipphonebookchoicecompdescr",
            name="value",
            field=models.CharField(max_length=80, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookchoicedigital",
            name="value",
            field=models.CharField(max_length=1, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookchoicehistname",
            name="value",
            field=models.CharField(max_length=30, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookchoicehotline",
            name="value",
            field=models.CharField(max_length=1, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookchoicenameadd",
            name="value",
            field=models.CharField(max_length=5, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookchoicenumbertype",
            name="value",
            field=models.CharField(max_length=1, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookchoicepfxln",
            name="value",
            field=models.CharField(max_length=10, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookchoiceprint",
            name="value",
            field=models.CharField(max_length=1, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookchoicetag",
            name="value",
            field=models.CharField(max_length=50, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookchoicetitle",
            name="value",
            field=models.CharField(max_length=45, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookchoiceusage",
            name="value",
            field=models.CharField(max_length=1, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="digital_allowed",
            field=models.ForeignKey(
                default=2,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoiceDigital",
            ),
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="hotline_allowed",
            field=models.ForeignKey(
                default=2,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoiceHotline",
            ),
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="inverse_search_allowed",
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="number_blocked",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="print_allowed",
            field=models.ForeignKey(
                default=2,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoicePrint",
            ),
        ),
        migrations.AlterField(
            model_name="voipphonebookdata",
            name="usage",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookChoiceUsage",
            ),
        ),
        migrations.AlterField(
            model_name="voipphonebookpartner",
            name="name",
            field=models.CharField(max_length=100, unique=True),
        ),
        migrations.AlterField(
            model_name="voipphonebookpartnerdeteme",
            name="entry",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="django_voipstack_phonebook.VoipPhonebookData",
                unique=True,
            ),
        ),
    ]
